﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flowers
{
    public class Lily: NaturalFlower
    {
        private double _bulbDiameter;

        public double BulbDiameter
        {
            get { return _bulbDiameter; }
            set { _bulbDiameter = value; }
        }
    }
}
