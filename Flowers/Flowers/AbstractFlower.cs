﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flowers
{
    public abstract class AbstractFlower
    {
        private string _name;
        private string _color;
        private double _price;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string Color
        {
            get { return _color; }
            set { _color = value; }
        }

        public double Price
        {
            get { return _price; }
            set { _price = value; }
        }
    }
}
