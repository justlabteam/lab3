﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flowers
{
    public class Rose: NaturalFlower
    {
        private double _spikeLength;

        public double SpikeLength
        {
            get { return _spikeLength; }
            set { _spikeLength = value; }
        }
    }
}
