﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flowers
{
    public class ArtificialFlower: AbstractFlower
    {
        private string _material;

        public string Material
        {
            get { return _material;}
            set { _material = value; }
        }
    }
}
