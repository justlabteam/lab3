﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flowers
{
    public class NaturalFlower: AbstractFlower
    {
        private int _freshness;

        public int Freshness
        {
            get { return _freshness; }
            set { _freshness = value; }
        }
    }
}
