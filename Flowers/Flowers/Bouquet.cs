﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flowers
{
    public class Bouquet
    {
        private List<AbstractFlower> listOfFlowers;

        public Bouquet()
        {
            listOfFlowers = new List<AbstractFlower>();
        }

        public void AddFlower(AbstractFlower f)
        {
            listOfFlowers.Add(f);
        }

        public void RemoveFlower(AbstractFlower f)
        {
            listOfFlowers.Remove(f);
        }
    }
}
